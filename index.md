---
layout: cv
title: Baptiste Dauphin
---
# Baptiste Dauphin
DevOps, AdminSys

<div id="webaddress">
	<a href="https://doc.baptiste-dauphin.com/">Ma doc</a> | <a href="mailto:baptistedauphin76@gmail.com">Mail</a> | <a href="https://linkedin.com/in/baptiste-dauphin/">LinkedIn</a> |  06 81 65 72 22 | 29 ans | Permis B
</div>


## Expériences

`avril 2022 - juin 2023`
### __DevOps Specialist__ | [Paygreen](https://www.paygreen.io/)

Travail en conditions hautement fiables (FinTech)

Travail en cloud (GCP)

Mise en conformité PCI-DSS de l'infrastructure

Amélioration des temps de mise en production

Ajout d'observabilité (dashboards) & alerting

Gestion des affaires courantes des opérations & support aux developpeurs

Améliorations des CI/CD

Refonte, design et sécurisation des flux de sorties (via proxy sur kubernetes)

Amélioration fiabilité infra (HPA, Resources, analyses réseaux)

Standardisation des logs


`janvier 2018 - mai 2021`
### __Administrateur systèmes et réseaux__ | [Qwant](https://www.qwant.com/)


Mise en production (data, schema, répartition de charge, code, certificats).

Automatisation : orchestration, déploiement, centralisation de logs, firewalling, monitoring.

Refonte et maintien des bases de données en cluster (haute disponibilité).

Refonte de l'architecture DNS (orchestrée).

Design et déploiement de l'infrastructure DNS QwantJunior.

Monitoring interne et externe : collecte de data + creation de dashboards automatisés.

Integration et maintenance des applications internes.

Création, entretien et mise à jour du parc de serveurs.

Répartition de charges avec health-checking et failover.

Support aux developpeurs et gestion des accès.

Prise en charge du POC avec OVH pendant 6 mois.

Maintien de la production (Running).

Industrialisation du legacy.


`février 2017 - janvier 2018`
### __Ingénieur d’Affaires__ | [Quantic Telecom](https://www.quantic-telecom.net/)

Mise en place de partenariats de revente et gestion des sous-traitants

Prospection directe et téléphonique, prise de rendez-vous, relance, négociation

Arrivée, câblage et configuration de matériels actifs et passifs optiques en datacenter

`janvier 2016 - février 2017`
### __Ingénieur Développement et Réseaux__ | [Groupe Renault](https://group.renault.com/groupe/implantations/nos-implantations-industrielles/usine-cleon/)

Réalisation d’un état de l’art et d’un cahier des charges précis

Analyse du flux réseau : (dés)encapsulation bit à bit

Automatisation de la remontée de défauts en mode asynchrone et envoi normé à l’interface graphique

`juin 2016 - septembre 2016`
### __Développeur Arduino__ | [Universidade do Algarve](https://www.ualg.pt/pt)

Rédation d’une base de connaissance pour un projet de recherche, création d’outils de visualisation

Rédaction d’une documentation complète en anglais

## Compétences informatiques

### OS
Debian/Ubuntu, ArchLinux, ESX.

### Infrastructure
OVH, Outscale, Gandi, VMWare, OpenStack, Terraform, Vault, cloud-init, PfSense, SaltStack, Ansible, GitLab / runner / s3, Docker, Swarm, Kubernetes, Python virtualenv, uwsgi, Php-fpm, Npm, HAProxy, NGINX (+naxsi WAF), Bind9, Unbound, Knot, PowerDNS, stack Elastic Search, RabbitMQ, MySQL/mariaDB, Percona Cluster, Redis, Grafana, Prometheus, Zabbix, Telegraf, InfluxDB, OpenSSL, lvm2, Squid, Keepalived, Bird, Syslog-ng, SystemD, Xinetd, Health-Checking.

### Réseaux
Iptables statefull et stateless, nmap, Tcpdump, Netcat, Mtr.

### Langages
Bash, Python, Jinja2, Yaml.

### Documentation personnelle
[Ma doc Linux](https://doc.baptiste-dauphin.com/)

## Formations

`2014-2017`
__École Supérieure d’Ingénieurs en Génie Électrique [(ESIGELEC)](http://www.esigelec.fr/fr), Rouen__

- Dominante : Affaires dans l'informatique et les réseaux.
- Systèmes d’exploitation, réseaux, gestion de projet, électronique, appel d’offres.

`2012-2014`
__[Institut Universitaire de Technologie Réseaux et Télécommunications](http://iutrouen.univ-rouen.fr/dut-reseaux-et-telecommunications-351515.kjsp), Elbeuf__

- Télécommunications, administration des réseaux, développement, bases de données, électronique.

`2010-2012`
__[Lycée Blaise Pascal](http://pascal-lyc.spip.ac-rouen.fr/), Rouen__

- Bac scientifique
- Option sciences de l'ingénieur : mecanique, électronique et informatique

## Langues

### Français
Langue maternelle

### Anglais
TOEIC 2017 : Working proficiency plus


## Travaux

### Recherche

`2016`
Comment générer différents signaux à partir d'une carte électronique Arduino, [documentation](https://drive.google.com/open?id=0B5wrnG1NJCSSQXNkTV9iaHdkamM)


## Centres d'intérêts

### Sports

Block, planche à voile, surf, snowboard.

### Informatique

Linux, cybersécurité, Root-Me, création de challenge pour [Le Hack](https://lehack.org/fr#MORE).

### Autres

Piano, lecture, communication non-violente, association e-sport,
