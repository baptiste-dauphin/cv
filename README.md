[![pipeline status](https://gitlab.com/baptiste-dauphin/cv/badges/master/pipeline.svg)](https://gitlab.com/baptiste-dauphin/cv/-/commits/master)

Forked from [elipapa project](http://elipapa.github.io/markdown-cv)


### Change the style

The included CSS renders CV in different styles:

- `kjhealy` the original default, inspired by [kjhealy's vita
template](https://github.com/kjhealy/kjh-vita)
- `davewhipp` is a tweaked version of `kjhealy`, with bigger fonts and dates
  right aligned

To change the default style, one needs to simply change the variable in the
`_config.yml` file.

Any other styling is possible. More CSS style contributions and forks are welcome!


### Author

Eliseo Papa ([Twitter](http://twitter.com/elipapa)/[GitHub](http://github.com/elipapa)/[website](https://elipapa.github.io)).

![Eliseo Papa](https://s.gravatar.com/avatar/eae1f0c01afda2bed9ce9cb88f6873f6?s=100)

### License

[MIT License](https://github.com/elipapa/markdown-cv/blob/master/LICENSE)
